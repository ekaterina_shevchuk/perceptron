#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include "DataMatrix.h"
using namespace std;

DataMatrix::~DataMatrix() {
	for (int i = 0; i < Matrix.size(); i++)
		Matrix[i].erase(Matrix[i].begin(), Matrix[i].end());
	Matrix.erase(Matrix.begin(), Matrix.end());
}

DataMatrix::DataMatrix(string dataFile) {
	ifstream file;
	string line, elem, part1F, part2F;
	int part1, part2, count = 0;
	float elemNUM;
	vector <string> Row;
	vector<float> RowF;
	file.open(dataFile);
	if (file.is_open()) {
		while (getline(file, line)) {
			istringstream Iss(line);
			while (Iss.good()) {
				getline(Iss, elem, ' ');
				if (elem == "") continue;
				elem.erase(remove(elem.end() - 1, elem.end(), ' '), elem.end());
				if (count != 0) {
					if (elem == "Nan") {
						RowF.push_back(0);
					}
					else {
						istringstream par(elem);
						getline(par, part1F, ',');
						getline(par, part2F);
						if (part2F.size() == 1) {
							elemNUM = stof(part1F) + ((float)stof(part2F) / 10);
						}
						if (part2F.size() == 2) {
							elemNUM = stof(part1F) + ((float)stof(part2F) / 100);
						}
						if (part2F.size() == 3) {
							elemNUM = stof(part1F) + ((float)stof(part2F) / 1000);
						}
						if (part2F.size() == 4) {
							elemNUM = stof(part1F) + ((float)stof(part2F) / 10000);
						}
						RowF.push_back(elemNUM);
					}
				}
				Row.push_back(elem);
			}

			if (line.length()) {
				if (count != 0) {
					MatrixF.push_back(RowF);
					RowF.erase(RowF.begin(), RowF.end());
				}
				Matrix.push_back(Row);
				num_rows++;
				Row.erase(Row.begin(), Row.end());
			}
			count++;
		}
	}
	num_atr = Matrix[0].size() - 2;
	standardization();
}

int DataMatrix::Get_num_atr() {
	return num_atr;
}

int DataMatrix::Get_num_examples() {
	return num_rows - 1;
}



float* DataMatrix::Get_float_value_row(int numRow) {
	float* value = (float*)malloc(num_atr*sizeof(float));
	for (int i = 0; i < num_atr; i++) {
		value[i] = MatrixF[numRow][i];
	}
	return value;
}


vector <float> DataMatrix::Get_answ_class(int numRow) {
	vector<float> value;
	for (int i = num_atr; i < num_atr + 2; i++) {
		value.push_back(MatrixF[numRow][i]);
	}
	return value;
}

void DataMatrix::standardization() {
	vector<float> value;
	float max1 = 0, max2 = 0;
	for (int j = 0; j < num_rows-1; j++) {
			if (MatrixF[j][num_atr] > max1) {
				max1 = MatrixF[j][num_atr];
			}
			if (MatrixF[j][num_atr+1] > max2) {
				max2 = MatrixF[j][num_atr+1];
			}
	}
	for (int j = 0; j < num_rows-1; j++) {
		MatrixF[j][num_atr] = ((float)MatrixF[j][num_atr])/max1;
		MatrixF[j][num_atr + 1] = ((float)MatrixF[j][num_atr+1]) / max2;
	}
}