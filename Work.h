#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include "DataMatrix.h"


class Work {
public:
	
	Work();
	~Work();

	float* performance(float* x);
	void teach(int m, DataMatrix dataMatrix);
	float countError(DataMatrix dataMatrix);
	void printTestError(DataMatrix dataMatrix);

private:

	const int numN[3] = {70, 90, 2}; //90 . 100
	const int numInput = 28;
	const float speed = 0.01;
	const int numLayers = 3;
	const int numExample = 65;

	//A = 1

	float*** W;
	float** error;
	float** S;
	float** out;
	float* errorWeb;
	
	//DataMatrix dataMatrix;
	
	vector<vector<vector<float>>> Wvect;

};