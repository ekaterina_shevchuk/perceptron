#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <cstdlib>
#include <algorithm>
#include "DataMatrix.h"
#include "Work.h"

using namespace std;

Work::Work() {
	//dataMatrix = dataMatrix1;
	vector<vector<float>> tempW1;
	vector<float> tempW0;
	W = (float***)calloc(numLayers, sizeof(float**));
	
	S = (float**)calloc(numLayers, sizeof(float*));
	out = (float**)calloc(numLayers, sizeof(float*));
	for (int i = 0; i < numLayers; i++) {
		W[i] = (float**)calloc(numN[i], sizeof(float*));
		
		for (int j = 0; j < numN[i]; j++) {
			int numInputTmp;
			if (i == 0) {
				numInputTmp = numInput;
				W[i][j] = (float*)calloc(numInput, sizeof(float));
			}
			else {
				numInputTmp = numN[i - 1];
				W[i][j] = (float*)calloc(numN[i - 1], sizeof(float));
			}
			for (int k = 0; k < numInputTmp; k++) {
				if (k % 3 == 0) {
					W[i][j][k] = 0.04;
				}
				if (k % 3 == 1) {
					W[i][j][k] = 0.05;
				}
				if (k % 3 == 2) {
					W[i][j][k] = 0.06;
				}
				/*W[i][j][k] = (rand() % 2) - 0.5*(rand() % 2) ;
				if (W[i][j][k] == 1) {
					W[i][j][k] -= 0.5;
				}*/
				tempW0.push_back(W[i][j][k]);
			}
			tempW1.push_back(tempW0);
			tempW0.erase(tempW0.begin(), tempW0.end());
		}
		Wvect.push_back(tempW1);
		tempW1.erase(tempW1.begin(), tempW1.end());
	}
}

float* Work::performance(float* x) {
	//inputLayers.push_back(x);
	vector<float> input;

	for (int l = 0; l < numLayers; l++) {
		
		S[l] = (float*)calloc(numN[l], sizeof(float));
		out[l] = (float*)calloc(numN[l], sizeof(float));
		for (int j = 0; j < numN[l]; j++) {
			int numInputTmp;
			if (l == 0) {
				numInputTmp = numInput;

				for (int i = 0; i < numInputTmp; i++) {
					S[l][j] += (x[i] * W[l][j][i]);
				}
			}
			else {
				numInputTmp = numN[l - 1];

				for (int i = 0; i < numInputTmp; i++) {
					S[l][j] += (out[l-1][i] * W[l][j][i]);
				}
				/*input.push_back({}); //����� �� ��� ??
				for (int t = 0; t < numN[l]; t++) {
					inputLayers[l].push_back(out[l - 1][t]);
				}*/
			}
			out[l][j] = 1 / (1 + exp((S[l][j])*(-1)));//*(-1)
		}
	}

	return out[numLayers - 1];

}

void Work::teach(int m, DataMatrix dataMatrix) {

	float* y = (float*)calloc(numN[numLayers-1], sizeof(float));
	
	errorWeb = (float*)calloc(m, sizeof(float));
	error = (float**)calloc(numLayers, sizeof(float*));
	for (int epoch = 0; epoch < m; epoch++) {

		for (int index = 0; index < numExample; index++) {
			for (int i = 0; i < numLayers; i++) {
				error[i] = (float*)calloc(numN[i], sizeof(float));
			}
			//int numExample = (rand() % 64) + 2;
			int numExample = index;
			float* value = dataMatrix.Get_float_value_row(numExample);
			vector<float> answer = dataMatrix.Get_answ_class(numExample);
			y = performance(value);

			for (int l = (numLayers - 1); l >= 0; l--) {
 				for (int k = 0; k < numN[l]; k++) {
					//y = performance(value);
					if (l == (numLayers - 1)&&(answer[k] != 0)) {
						error[l][k] = (y[k] - answer[k])*(y[k] * (1 - y[k]));
					}
					else if (l != (numLayers - 1)){
						int numInputTmp = numN[l + 1];

						float summError = 0;
						for (int n = 0; n < numInputTmp; n++) {
							summError += error[l + 1][n] * W[l + 1][n][k];
						}
						if ((l != (numLayers - 1) ) || (l == (numLayers - 1) && (out[l][k] != 0))) {
							error[l][k] = summError *out[l][k] *(1 - out[l][k]); //// ��� ���������
							//error[l][k] = summError * (y[k] * (1 - y[k]));
						}
					}
				}
			}

			for (int l = 0; l < numLayers; l++) {
				float* res;
				int numInputTmp;
				if (l == 0) {
					res = value;
					numInputTmp = numInput;
				}
				else {
					numInputTmp = numN[l - 1];
					res = out[l - 1];
				}
				for (int j = 0; j < numN[l]; j++) {
					for (int i = 0; i < numInputTmp; i++) {
						//����� �� ����� ��������� �� fp, ��� �������, ��� ��� �������� ����� ���� ��������� �� out[l][k]
						float fp = out[l][j] * (1 - out[l][j]);
						W[l][j][i] += (-1)*speed*error[l][j] * res[i];//������ fp
						Wvect[l][j][i] = W[l][j][i];

					}
				}
			}
		}
		errorWeb[epoch] = countError(dataMatrix);
		//printTestError(dataMatrix);
		cout << "err" << epoch+1 << " " << errorWeb[epoch] << endl;
	}
}

float Work::countError(DataMatrix dataMatrix) {
	float s1 = 0, s2 = 0 ; 
	for (int i = 0; i < numExample; i++){
		float * neuron = performance(dataMatrix.Get_float_value_row(i));
		vector<float> value_class = dataMatrix.Get_answ_class(i);
		if (value_class[0] != 0) {
			s1 += ((neuron[0] - value_class[0])*(neuron[0] - value_class[0]));
		}
		if (value_class[1] != 0) {
			s2 += ((neuron[1] - value_class[1])*(neuron[1] - value_class[1]));
		}
	}
	float err1 = s1/2, err2 = s2/2;
	return max(err1, err2);
}

void Work::printTestError(DataMatrix dataMatrix) {
	vector<float> testData;
	float rmse1 = 0, rmse2 = 0;
	for (int i = 65; i < dataMatrix.Get_num_examples() - 1; i++) {
		float * neuron = performance(dataMatrix.Get_float_value_row(i));
		testData = dataMatrix.Get_answ_class(i);
		if (testData[0] != 0) {
			rmse1 += ((neuron[0] - testData[0])*(neuron[0] - testData[0]));
		}
		if (testData[1] != 0) {
			rmse2 += ((neuron[1] - testData[1])*(neuron[1] - testData[1]));
		}
	}
	rmse1 = sqrtf(rmse1 / 29);
	rmse2 = sqrtf(rmse2 / 29);

	cout << "RMSE1 = " << rmse1 << ", RMSE2 = " << rmse2 << endl;
	return;
}